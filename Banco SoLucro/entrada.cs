﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco_SoLucro
{
    public partial class entrada : UserControl
    {
        private static entrada _instance;
        public static entrada instance
        {
            get
            {
                if (_instance == null)
                    _instance = new entrada();
                return _instance;
            }
        }

        public void atualiza_textBox2(String valor)
        {
            this.textBox2.Text = valor;
        }

        public entrada()
        {
            InitializeComponent();
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            criar modal = new criar();
            modal.ShowDialog();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
