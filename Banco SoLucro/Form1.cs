﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Banco_SoLucro
{
    public partial class Banco : Form
    {
        public Banco()
        {
            InitializeComponent();
        }

        private static Banco _instance;
        public static Banco instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Banco();
                return _instance;
            }
        }

        //ao abrir aplicação, carregar a parte de design que corresponde aos menus de entrada
        protected override void OnLoad(EventArgs e)
        {

            panel2.Controls.Add(entrada.instance);
            entrada.instance.Dock = DockStyle.Fill;
            base.OnLoad(e);
        }


        //Ao pressionar a tecla Enter vai disparar a função de pesquisa
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
                bunifuFlatButton2_Click(sender, e);
            }

        }

        //Desativar som beep quando é pressionada a tecla Enter na caixa de texto
        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
            }

        }


        //Função de pesquisa quando é pressionada a tecla enter na caixa de texto
        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            //Verifica se existem letras na caixa de texto
            String mystring = textBox1.Text;
            int errorCounter = 0;
            for (int i = 0; i < mystring.Length; i++)

                errorCounter = Regex.Matches(mystring, @"[a-zA-Z]").Count;

            if (errorCounter > 0)
            {
                entrada.instance.atualiza_textBox2("Introduza apenas números!");
            }

            //Verifica de a caixa de texto está em branco
            if (String.IsNullOrEmpty(textBox1.Text))
            {
                entrada.instance.atualiza_textBox2("Por favor introduza um nr de conta!");
            }
            else

            //Se houver conteúdo válido (somente numeros), fazer busca na base de dados
            {
                String constring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb";
                String sql = "SELECT Count(*) FROM tblDados WHERE NrConta=" + textBox1.Text + "";

                using (OleDbConnection con = new OleDbConnection(constring))
                using (OleDbCommand cmd = new OleDbCommand(sql, con))

                    try
                    {

                        con.Open();

                        
                        int result = (int)cmd.ExecuteScalar();
                        if (result > 0)
                        {
                            //Se o resultado for positivo, troca o design para vista de conta
                            panel2.Controls.Add(conta.instance);
                            conta.instance.Dock = DockStyle.Fill;
                            conta.instance.BringToFront();
                            conta.instance.atualiza_textBox2("Login efetuado com sucesso!!");
                            conta.instance.atualizaNrConta(this.textBox1.Text);
                            this.textBox1.Enabled = false;

                        }
                        else

                        //Se o resultado for negativo, apresenta mensagem de erro
                        {
                            entrada.instance.atualiza_textBox2("Conta inválida!!");

                            con.Close();
                        }


                    }


                    catch (Exception ex)
                    {

                        con.Close();
                    }

            }

            
                
        }








    }
}
