﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco_SoLucro
{
    public partial class conta : UserControl
    {
        public conta()
        {
            InitializeComponent();
        }



        private static conta _instance;
        public static conta instance
        {
            get
            {
                if (_instance == null)
                    _instance = new conta();
                return _instance;
            }
        }


        public string TextBoxValue
        {
            get { return textBox2.Text; }
            set { textBox2.Text = value; }
        }

        public void atualiza_textBox2( String valor)
        {
            this.textBox2.Text = valor;    
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            if (this.bunifuFlatButton2.Visible == true)
            {
                this.bunifuFlatButton2.Visible = false;
                this.bunifuFlatButton3.Visible = false;
                this.textBox1.Text = "";
                this.textBox1.Enabled = true;
                this.textBox1.Focus();
                //saldo


                String constring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb";
                String sql = "SELECT Valor FROM tblDados WHERE NrConta=" + textBox4.Text + "";
                String valor;
                using (OleDbConnection con = new OleDbConnection(constring))
                using (OleDbCommand cmd = new OleDbCommand(sql, con))

                    try
                    {

                        con.Open();


                        var result = cmd.ExecuteScalar();

                        this.textBox3.Text = result.ToString();

                    }


                    catch (Exception ex)
                    {

                        con.Close();
                    }
            } 
            else
            {
                

                if (textBox1.Text == "")
                {
                    this.textBox2.Text = "Por favor introduza um valor!!";
                }
                else
                {
                    int valor1 = Convert.ToInt32(textBox1.Text);
                    int valor2 = Convert.ToInt32(textBox3.Text);

                    if (valor1 <= 0)
                    {
                        this.textBox2.Text = "Valor inválido para depósito";
                    }
                    else
                    {
                        int deposaldo = valor2 + valor1;
                        this.textBox3.Text = deposaldo.ToString();
                        this.textBox2.Text = "Depósito de " + valor1 + "$ efetuado com sucesso!!";
                        this.updateSaldo(deposaldo.ToString(), textBox4.Text);
                        this.textBox1.Text = "";
                    }

                }

                
                    

            }


            
        }


        //Função para transmitir o nr de conta corrente para o submenu
        public void atualizaNrConta(String valor)
        {
          
            this.textBox4.Text = valor;
        }


        //funçao para update do valor de deposito
        public void updateSaldo(string valor, string nrconta)
        {

            String constring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb";
            String sql = "UPDATE tblDados SET Valor=" + valor + " WHERE NrConta=" + nrconta + "";
            using (OleDbConnection con = new OleDbConnection(constring))
            using (OleDbCommand cmd = new OleDbCommand(sql, con))

            try
            {
                con.Open();

                    cmd.ExecuteNonQuery();
                    
            }
            catch (Exception expe)
            {
                MessageBox.Show(expe.Source);
            }
            
        }

        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = "";
            this.textBox2.Text = "";
            this.bunifuFlatButton1.Visible = true;
            this.bunifuFlatButton2.Visible = true;
            this.bunifuFlatButton3.Visible = true;
            this.bunifuFlatButton4.Visible = true;
            this.textBox2.Text = "Finalizado com Sucesso!!";
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {


            if (this.bunifuFlatButton1.Visible == true)
            {
                this.bunifuFlatButton1.Visible = false;
                this.bunifuFlatButton3.Visible = false;
                this.textBox1.Text = "";
                this.textBox1.Enabled = true;
                this.textBox1.Focus();
                //saldo


                String constring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb";
                String sql = "SELECT Valor FROM tblDados WHERE NrConta=" + textBox4.Text + "";
                String valor;
                using (OleDbConnection con = new OleDbConnection(constring))
                using (OleDbCommand cmd = new OleDbCommand(sql, con))

                    try
                    {

                        con.Open();


                        var result = cmd.ExecuteScalar();

                        this.textBox3.Text = result.ToString();

                    }


                    catch (Exception ex)
                    {

                        con.Close();
                    }
            }
            else
            {


                if (textBox1.Text == "")
                {
                    this.textBox2.Text = "Por favor introduza um valor!!";
                }
                else
                {
                    int valor1 = Convert.ToInt32(textBox1.Text);
                    int valor2 = Convert.ToInt32(textBox3.Text);

                    if (valor1 <= 0)
                    {
                        this.textBox2.Text = "Valor inválido para saque.";
                    }
                    if(valor1 > valor2)
                    {
                        this.textBox2.Text = "Saldo insuficiente para saque!";
                    }


                    if(valor1 <= valor2)
                    {
                        int deposaldo = valor2 - valor1;
                        this.textBox3.Text = deposaldo.ToString();
                        this.textBox2.Text = "Saque de " + valor1 + "$ efetuado com sucesso!!";
                        this.updateSaldo(deposaldo.ToString(), textBox4.Text);
                        this.textBox1.Text = "";
                    }

                }




            }

        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            if (this.bunifuFlatButton1.Visible == true)
            {
                this.bunifuFlatButton1.Visible = false;
                this.bunifuFlatButton2.Visible = false;
                this.textBox1.Text = "";
                this.textBox1.Enabled = true;
                this.textBox1.Focus();
                //saldo
                String constring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb";
                String sql = "SELECT Valor FROM tblDados WHERE NrConta=" + textBox4.Text + "";
                String valor;
                using (OleDbConnection con = new OleDbConnection(constring))
                using (OleDbCommand cmd = new OleDbCommand(sql, con))

                    try
                    {

                        con.Open();


                        var result = cmd.ExecuteScalar();

                        this.textBox3.Text = result.ToString();
                        this.textBox2.Text = "Consulta de Saldo";
                        this.textBox1.Enabled = false;

                    }


                    catch (Exception ex)
                    {

                        con.Close();
                    }




            }

        }





        }
}
