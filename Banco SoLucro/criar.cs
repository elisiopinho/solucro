﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco_SoLucro
{
    public partial class criar : Form
    {
        public criar()
        {
            InitializeComponent();
        }




        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            OleDbConnection conn = new OleDbConnection();
            conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb";

            String nome = textBox1.Text;
            String valor = textBox2.Text;
            int nrconta = GenerateRandomNo();


            OleDbCommand cmd = new OleDbCommand("INSERT INTO tblDados (NrConta, Nome, Valor) Values(@nrconta, @Nome, @Valor)");
            cmd.Connection = conn;

            conn.Open();

            if (conn.State == System.Data.ConnectionState.Open && (textBox1.Text) != "" && (textBox2.Text != ""))
            {
                cmd.Parameters.Add("@NrConta", OleDbType.Integer).Value = nrconta;
                cmd.Parameters.Add("@Nome", OleDbType.VarChar).Value = nome;
                cmd.Parameters.Add("@Valor", OleDbType.VarChar).Value = valor;
                

                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Conta salva com Sucesso\nAnote o número da conta\nNúmero:" + nrconta, "Registo de Conta", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    textBox1.Text = "";
                    textBox2.Text = "";

                    conn.Close();
                    this.Close();
                }
                catch (OleDbException ex)
                {
                    MessageBox.Show(ex.Source);
                    conn.Close();
                }
            }
            else
            {
                MessageBox.Show("Preencher os dois campos!");
            }
        }



        public int GenerateRandomNo()
        {
            int _min = 10000;
            int _max = 99999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }


    }
}
